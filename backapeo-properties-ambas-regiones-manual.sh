#!/bin/bash

# Este escript agilisa el copiado o respaldo de los archivos hacia una nueva carpeta
# Deberas poner por cada region el nombbre de carpeta backup y los nombres de cada
# archivo para copiar.
# Creacion de carpetas para ambas regiones

mkdir REGION_CU1 REGION_EU2
cd REGION_CU1

# Logueo a BITBUCKET
git config --global http.sslVerify false
git config --global user.name "T11238"
git config --global user.email jenrrysoto@bcp.com.pe

echo -e " "
echo -e "---------------------------------------"
echo -e "DESCARGANDO PROPERTIES DE LA REGION CU1"
echo -e "---------------------------------------"
# CERTI REGION_CU1
git clone https://T11238@bitbucket.lima.bcp.com.pe/scm/yape-cfg/yape-services-properties-cer.git -b yape-1.0-azure-cu1

echo -e " "
IFS=" "  # Cambia el separador de campos interno a espacio en blan
read -p "Ingresa el nombre de la carpeta para crear el backup de la region CU1; " carpeta_cu1
echo -e "-----------------------------------------------------------------------"
read -p "Ingrese los nombres de los arcivos para respaldar, separados por un espacio; " -a respaldo_cu1
echo -e " "

mkdir $carpeta_cu1

for archivo_cu1 in "${respaldo_cu1[@]}"; do
    cp "yape-services-properties-cer/$archivo_cu1" "$carpeta_cu1"
done

cp  yape-services-properties-cer/$respaldo_cu1 $carpeta_cu1

echo -e " "
echo -e "LOS ARCHIVOS BACKAPEADOS DE LA REGION CU1 SON; "
ls -1 $carpeta_cu1/* | cat -n
echo -e "------------------------------------------------"
cd ../REGION_EU2

echo -e "DESCARGANDO PROPERTIES DE LA REGION EU2"
echo -e "---------------------------------------"
# CERTI REGION_EU2
git clone https://T11238@bitbucket.lima.bcp.com.pe/scm/yape-cfg/yape-services-properties-cer.git -b yape-1.0-azure

echo -e " "
IFS=" "  # Cambia el separador de campos interno a espacio en blanco
read -p "Ingresa el nombre de la carpeta para crear el backup de la region EU2; " carpeta_eu2
echo -e "-----------------------------------------------------------------------"
read -p "Ingrese los nombres de los arcivos para respaldar, separados por un espacio; " -a respaldo_eu2
echo -e " "

mkdir $carpeta_eu2

for archivo_eu2 in "${respaldo_eu2[@]}"; do
    cp "yape-services-properties-cer/$archivo_eu2" "$carpeta_eu2"
done

cp  yape-services-properties-cer/$respaldo_eu2 $carpeta_eu2

echo -e " "
echo -e "LOS ARCHIVOS BACKAPEADOS DE LA REGION EU2 SON; "
ls -1 $carpeta_eu2/* | cat -n
echo -e "-----------------------------------------------"
cd
echo "El backup para ambas reiones a terminado a las; $(date +%r)"

## Autor; Jenrry Soto Dextre
